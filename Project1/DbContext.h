#pragma once

#include <string>
#include <iostream>
#include <pqxx/pqxx>
#include "QueryResponse.h"

class DbContext {
private:
    const std::string ConnectionString
        = "host=localhost port=5432 dbname=bank user=postgres password=1";

public:
    QueryResponse RunSql(std::string query);
};