#pragma once
#include "DbContext.h"
#include "DepositType.h"
#include "Deposit.h"
#include "Helper.h"


class DepositService {
private:
	DbContext* _dbContext;
	Helper _helper;

public:
	DepositService();
	std::list<DepositType> GetDepositTypes();
};