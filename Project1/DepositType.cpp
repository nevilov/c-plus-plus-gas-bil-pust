#include "DepositType.h"

DepositType::DepositType()
{
}

DepositType::DepositType(int id, std::string name, double percent, time_t approvalDate)
{
	Id = id;
	Name = name;
	Percent = percent;
	ApprovalDate = approvalDate;
}
