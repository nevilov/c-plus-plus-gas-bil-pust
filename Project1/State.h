#pragma once
#include <string>
#include "User.h"
#include "Account.h"

class State
{
private:
    static State* p_instance;
    State() {};
    State(const State&);
    State& operator=(State&);

public:
    User CurrentUser;
    Account UserAccount;
    void SetUser(User user);
    void SetAccount(Account account);
    static State* getInstance() {
        if (!p_instance)
            p_instance = new State();
        return p_instance;
    }
};