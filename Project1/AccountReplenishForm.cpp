﻿#include "AccountReplenishForm.h"

System::Void Project1::AccountReplenishForm::button2_Click(System::Object^ sender, System::EventArgs^ e)
{
	this->Hide();
	HomeForm^ homeForm = gcnew HomeForm;
	homeForm->Show();
}

System::Void Project1::AccountReplenishForm::button1_Click(System::Object^ sender, System::EventArgs^ e)
{
	AccountService accountService;
	auto currentAccount = State::getInstance();
	
	auto amountRequested = System::Double::Parse(this->Amount->Text);
	
	if (amountRequested > 10000) {
		MessageBox::Show("Невозможно пополнить на более 10 000р за одну операцию",
			"Банк (оповещение)");

		return;
	}

	if (accountService.Replenish(currentAccount->UserAccount, amountRequested)) {

		MessageBox::Show("Счет успешно пополнен!", "Банк (оповещение)");

		this->Hide();
		HomeForm^ homeForm = gcnew HomeForm;
		homeForm->Show();
	}
}
