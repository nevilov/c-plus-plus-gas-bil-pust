#pragma once
#include <string>
#include <iostream>
#include <pqxx/pqxx>

class QueryResponse {
public:
    bool IsSuccess;
    pqxx::result Result;
    std::string ErrorMessage;

    QueryResponse();
    QueryResponse(bool isSuccess, pqxx::result result, std::string errorMessage);
};

