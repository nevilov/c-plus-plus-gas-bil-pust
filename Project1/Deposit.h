#pragma once
#include "EntityBase.h"

class Deposit : public EntityBase {
public:
	time_t CreatedDate;
	double Amount;
	int AccountId;
	int DepositTypeId;

	Deposit();
	Deposit(int id, time_t createdDate, double amount, int accountId, int depositType);
	Deposit(double amount, int accountId, int depositType);
};