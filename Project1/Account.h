#pragma once
#include "EntityBase.h"

class Account : public EntityBase {
public:
	int UserId;
	double Amount;
	int BankId;
	std::string CreatedAt;

	Account(int id, int userId, double amount, int bankId, std::string createdAt);
	Account(int userId, double amount, int bankId);
	Account();
};