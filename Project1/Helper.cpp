#include "Helper.h"

time_t Helper::parseDateFromDb(const std::string& dateStr)
{
    std::tm tm = {};
    std::istringstream ss(dateStr);
    ss >> std::get_time(&tm, "%Y-%m-%d");

    if (ss.fail()) {
        std::cerr << "Parse failed\n";
        return -1;
    }

    return mktime(&tm);
}
