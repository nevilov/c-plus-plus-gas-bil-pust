#pragma once

#include "DbContext.h"
#include "Account.h"
#include "State.h"
#include <string>

class AccountService {
private:
	DbContext* _dbContext;

public:
	AccountService();
	Account GetAccountByUser(int userId);
	bool Replenish(Account account, double amount);
	bool Widthdraw(Account account, double amount);
};