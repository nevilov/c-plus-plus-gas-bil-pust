#pragma once
#include "DbContext.h"
#include "State.h"
#include "User.h"
#include "AccountService.h"


class UserService {
private:
	DbContext* _dbContext;
	AccountService* _accoutService;

public:
	UserService();
	bool Authorize(std::string email, std::string password);
};