﻿#include "MainForm.h"

System::Void Project1::MainForm::AuthorizeButton_Click(System::Object^ sender, System::EventArgs^ e)
{
	UserService userService;

	array<Byte>^ bytes1 = System::Text::Encoding::UTF8->GetBytes(this->login->Text);
	array<Byte>^ bytes2 = System::Text::Encoding::UTF8->GetBytes(this->Password->Text);
	pin_ptr<Byte> pinnedBytes1 = &bytes1[0]; // Pin memory to avoid GC relocation
	pin_ptr<Byte> pinnedBytes2 = &bytes2[0]; // Pin memory to avoid GC relocation
	std::string email(reinterpret_cast<char*>(pinnedBytes1), bytes1->Length);
	std::string password(reinterpret_cast<char*>(pinnedBytes2), bytes2->Length);


	//std::string email = msclr::interop::marshal_as<std::string>(this->login->Text);
	//auto password = context.marshal_as<std::string>(this->Password->Text);
	if (!userService.Authorize(email, password)) {
		MessageBox::Show("Неверный логин или пароль!", "Банк (оповещение)");
		return;
	}

	this->Hide();
	HomeForm^ homeForm = gcnew HomeForm;
	homeForm->Show();
}
