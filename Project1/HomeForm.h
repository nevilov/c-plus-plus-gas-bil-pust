﻿#pragma once

#include "State.h"
#include "MainForm.h"
#include "AccountReplenishForm.h"
#include "AccountWithdrawForm.h"
#include "AddDepositForm.h"

namespace Project1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for HomeForm
	/// </summary>
	public ref class HomeForm : public System::Windows::Forms::Form
	{
	public:
		HomeForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			std::string greet = "Здравствуйте, " + State::getInstance()->CurrentUser.Name;
			String^ greetSystem = gcnew String(greet.c_str());
			this->GreetingLabel->Text = greetSystem;

			auto moneyAccount = State::getInstance()->UserAccount.Amount + " руб";
			this->MoneyLabel->Text = moneyAccount;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~HomeForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Panel^ panel2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Timer^ timer1;

	private: System::Windows::Forms::Panel^ panel3;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::DataGridView^ dataGridView1;
	private: System::Windows::Forms::Panel^ panel5;
	private: System::Windows::Forms::Panel^ panel4;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ MoneyLabel;
	private: System::Windows::Forms::Label^ GreetingLabel;
	private: System::Windows::Forms::FlowLayoutPanel^ flowLayoutPanel1;
	private: System::Windows::Forms::VScrollBar^ vScrollBar1;
	private: System::Windows::Forms::Panel^ panel6;
	private: System::Windows::Forms::Button^ OpenDeposit;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ ExitButton;
	private: System::Windows::Forms::Button^ button5;



	private: System::ComponentModel::IContainer^ components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(HomeForm::typeid));
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel5 = (gcnew System::Windows::Forms::Panel());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->MoneyLabel = (gcnew System::Windows::Forms::Label());
			this->GreetingLabel = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->panel4 = (gcnew System::Windows::Forms::Panel());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->vScrollBar1 = (gcnew System::Windows::Forms::VScrollBar());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->ExitButton = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->OpenDeposit = (gcnew System::Windows::Forms::Button());
			this->panel1->SuspendLayout();
			this->panel5->SuspendLayout();
			this->panel2->SuspendLayout();
			this->panel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->panel4->SuspendLayout();
			this->panel6->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->Controls->Add(this->panel5);
			this->panel1->Controls->Add(this->panel2);
			this->panel1->Location = System::Drawing::Point(2, 1);
			this->panel1->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(1564, 204);
			this->panel1->TabIndex = 0;
			// 
			// panel5
			// 
			this->panel5->Controls->Add(this->label3);
			this->panel5->Controls->Add(this->MoneyLabel);
			this->panel5->Controls->Add(this->GreetingLabel);
			this->panel5->Location = System::Drawing::Point(859, 0);
			this->panel5->Name = L"panel5";
			this->panel5->Size = System::Drawing::Size(705, 201);
			this->panel5->TabIndex = 2;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Impact", 12));
			this->label3->Location = System::Drawing::Point(12, 72);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(225, 39);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Денег на счету:";
			// 
			// MoneyLabel
			// 
			this->MoneyLabel->AutoSize = true;
			this->MoneyLabel->Font = (gcnew System::Drawing::Font(L"Impact", 12));
			this->MoneyLabel->Location = System::Drawing::Point(250, 72);
			this->MoneyLabel->Name = L"MoneyLabel";
			this->MoneyLabel->Size = System::Drawing::Size(35, 39);
			this->MoneyLabel->TabIndex = 1;
			this->MoneyLabel->Text = L"$";
			// 
			// GreetingLabel
			// 
			this->GreetingLabel->AutoSize = true;
			this->GreetingLabel->Font = (gcnew System::Drawing::Font(L"Impact", 12));
			this->GreetingLabel->Location = System::Drawing::Point(12, 21);
			this->GreetingLabel->Name = L"GreetingLabel";
			this->GreetingLabel->Size = System::Drawing::Size(222, 39);
			this->GreetingLabel->TabIndex = 0;
			this->GreetingLabel->Text = L"Здравствуйте, ";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->label1);
			this->panel2->Controls->Add(this->button2);
			this->panel2->Controls->Add(this->button1);
			this->panel2->Font = (gcnew System::Drawing::Font(L"Impact", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->panel2->Location = System::Drawing::Point(0, 3);
			this->panel2->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(854, 198);
			this->panel2->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(11, 29);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(298, 39);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Операции со счетом";
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->button2->Location = System::Drawing::Point(301, 91);
			this->button2->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(264, 62);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Пополнить счет";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &HomeForm::button2_Click);
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->button1->Location = System::Drawing::Point(18, 91);
			this->button1->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(258, 62);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Снять деньги";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &HomeForm::button1_Click);
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->label2);
			this->panel3->Location = System::Drawing::Point(2, 208);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(854, 827);
			this->panel3->TabIndex = 2;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Impact", 12));
			this->label2->Location = System::Drawing::Point(20, 48);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(191, 39);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Мои вклады";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToOrderColumns = true;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->dataGridView1->Location = System::Drawing::Point(0, 0);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->RowHeadersWidth = 82;
			this->dataGridView1->RowTemplate->Height = 33;
			this->dataGridView1->Size = System::Drawing::Size(854, 713);
			this->dataGridView1->TabIndex = 3;
			// 
			// panel4
			// 
			this->panel4->Controls->Add(this->flowLayoutPanel1);
			this->panel4->Controls->Add(this->vScrollBar1);
			this->panel4->Controls->Add(this->dataGridView1);
			this->panel4->Location = System::Drawing::Point(2, 319);
			this->panel4->Name = L"panel4";
			this->panel4->Size = System::Drawing::Size(854, 713);
			this->panel4->TabIndex = 3;
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Location = System::Drawing::Point(826, 148);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(14, 8);
			this->flowLayoutPanel1->TabIndex = 5;
			// 
			// vScrollBar1
			// 
			this->vScrollBar1->Location = System::Drawing::Point(804, 0);
			this->vScrollBar1->Name = L"vScrollBar1";
			this->vScrollBar1->Size = System::Drawing::Size(50, 716);
			this->vScrollBar1->TabIndex = 4;
			// 
			// panel6
			// 
			this->panel6->Controls->Add(this->button5);
			this->panel6->Controls->Add(this->ExitButton);
			this->panel6->Controls->Add(this->button4);
			this->panel6->Controls->Add(this->button3);
			this->panel6->Controls->Add(this->OpenDeposit);
			this->panel6->Location = System::Drawing::Point(859, 208);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(707, 824);
			this->panel6->TabIndex = 4;
			// 
			// button5
			// 
			this->button5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button5->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->button5->Location = System::Drawing::Point(0, 213);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(704, 74);
			this->button5->TabIndex = 4;
			this->button5->Text = L"Информация о банке";
			this->button5->UseVisualStyleBackColor = true;
			// 
			// ExitButton
			// 
			this->ExitButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->ExitButton->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->ExitButton->Location = System::Drawing::Point(3, 753);
			this->ExitButton->Name = L"ExitButton";
			this->ExitButton->Size = System::Drawing::Size(704, 74);
			this->ExitButton->TabIndex = 3;
			this->ExitButton->Text = L"Выйти";
			this->ExitButton->UseVisualStyleBackColor = true;
			this->ExitButton->Click += gcnew System::EventHandler(this, &HomeForm::button5_Click);
			// 
			// button4
			// 
			this->button4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button4->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->button4->Location = System::Drawing::Point(2, 142);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(704, 74);
			this->button4->TabIndex = 2;
			this->button4->Text = L"Расчитать прибыль";
			this->button4->UseVisualStyleBackColor = true;
			// 
			// button3
			// 
			this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button3->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->button3->Location = System::Drawing::Point(3, 74);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(704, 74);
			this->button3->TabIndex = 1;
			this->button3->Text = L"Закрыть вклад";
			this->button3->UseVisualStyleBackColor = true;
			// 
			// OpenDeposit
			// 
			this->OpenDeposit->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->OpenDeposit->Font = (gcnew System::Drawing::Font(L"Impact", 10));
			this->OpenDeposit->Location = System::Drawing::Point(3, 3);
			this->OpenDeposit->Name = L"OpenDeposit";
			this->OpenDeposit->Size = System::Drawing::Size(704, 74);
			this->OpenDeposit->TabIndex = 0;
			this->OpenDeposit->Text = L"Открыть вклад";
			this->OpenDeposit->UseVisualStyleBackColor = true;
			this->OpenDeposit->Click += gcnew System::EventHandler(this, &HomeForm::OpenDeposit_Click);
			// 
			// HomeForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(10, 26);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1566, 1031);
			this->Controls->Add(this->panel6);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->panel4);
			this->Controls->Add(this->panel3);
			this->Font = (gcnew System::Drawing::Font(L"Impact", 7.875F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->ImeMode = System::Windows::Forms::ImeMode::Disable;
			this->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->Name = L"HomeForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ОАО Банк \"СевГУ\"";
			this->panel1->ResumeLayout(false);
			this->panel5->ResumeLayout(false);
			this->panel5->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->panel4->ResumeLayout(false);
			this->panel6->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void OpenDeposit_Click(System::Object^ sender, System::EventArgs^ e);
};
}
