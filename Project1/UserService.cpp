#include "UserService.h"

UserService::UserService() {
	_dbContext = new DbContext();
	_accoutService = new AccountService();
}

bool UserService::Authorize(std::string email, std::string password) {

	std::string query = std::format(
		"select id, email, passport, name, birthdate from users where email = '{0}' and password = '{1}'", email, password);
	QueryResponse result = _dbContext->RunSql(query);

	if (!result.IsSuccess || result.Result.size() < 1) {
		return false;
	}

	std::cout << "Id: " << result.Result[0][0] << " Email: " << result.Result[0][1] << std::endl;

	int id = from_string<int>(result.Result[0][0]);
	User user(id, to_string(result.Result[0][3]), to_string(result.Result[0][2]), password, email);
	State::getInstance()->SetUser(user);

	auto userAccount = _accoutService->GetAccountByUser(id);
	State::getInstance()->SetAccount(userAccount);

	return true;
}