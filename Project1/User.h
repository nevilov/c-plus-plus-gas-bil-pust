#pragma once

#include "Person.h"

class User : public Person {
public:
	std::string Password;
	std::string Email;

	User(int id,
		std::string name,
		std::string passport,
		std::string password,
		std::string email);
	User();
};