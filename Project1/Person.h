#pragma once

#include<iostream>
#include "EntityBase.h"
#include <ctime>

class Person : public EntityBase {
public:
	std::string Name;
	std::string Passport;

	Person(int id,
		std::string name,
		std::string passport);

	Person();
};