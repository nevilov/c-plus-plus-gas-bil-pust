﻿#include "HomeForm.h"

System::Void Project1::HomeForm::button1_Click(System::Object^ sender, System::EventArgs^ e)
{
	this->Hide();
	AccountWithdrawForm^ accountWithdrawForm = gcnew AccountWithdrawForm;
	accountWithdrawForm->Show();
}

System::Void Project1::HomeForm::button5_Click(System::Object^ sender, System::EventArgs^ e)
{
	State::getInstance()->CurrentUser = User();
	State::getInstance()->UserAccount = Account();
	this->Hide();
	MainForm^ mainForm = gcnew MainForm;
	mainForm->Show();
}

System::Void Project1::HomeForm::button2_Click(System::Object^ sender, System::EventArgs^ e)
{
	this->Hide();
	AccountReplenishForm^ accountReplenishForm = gcnew AccountReplenishForm;
	accountReplenishForm->Show();
}

System::Void Project1::HomeForm::OpenDeposit_Click(System::Object^ sender, System::EventArgs^ e)
{
	this->Hide();
	AddDepositForm^ addDepositForm = gcnew AddDepositForm;
	addDepositForm->Show();
}

