#include "DepositService.h"
#include "ParseDateFromDb.cpp"

DepositService::DepositService()
{
	_dbContext = new DbContext();
	_helper = Helper();
}

std::list<DepositType> DepositService::GetDepositTypes()
{
	auto query = "SELECT id, name, percent, approvaldate FROM public.deposittypes;";
	auto queryResult = _dbContext->RunSql(query);
	
	if (!queryResult.IsSuccess) {
		return std::list<DepositType>();
	}

	auto response = queryResult.Result;
	std::list<DepositType> depositTypes;

	for (size_t i = 0; i < response.size(); i++)
	{

		std::cout << "id: " << response[i][0]
			<< " name: " << response[i][1]
			<< " percent: " << response[i][2]
			<< " approvaldate: " << response[i][3] << std::endl;

		int id = pqxx::from_string<int>(response[i][0]);
		std::string name = to_string(response[i][1]);
		double percent = pqxx::from_string<double>(response[i][2]);
		time_t approvalDate = _helper.parseDateFromDb(response[i][3].c_str());

		DepositType depositType(id, name, percent, approvalDate);


		depositTypes.push_front(depositType);
	}

	return depositTypes;
}
