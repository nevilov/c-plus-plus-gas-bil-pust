#pragma once
#include "EntityBase.h"
#include <ctime>

class DepositType : public EntityBase {
public:
	std::string Name;
	double Percent;
	time_t ApprovalDate;

	DepositType();
	DepositType(int id, std::string name, double percent, time_t approvalDate);
};