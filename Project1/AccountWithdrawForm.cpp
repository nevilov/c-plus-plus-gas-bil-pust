﻿#include "AccountWithdrawForm.h"

System::Void Project1::AccountWithdrawForm::button2_Click(System::Object^ sender, System::EventArgs^ e)
{
	this->Hide();
	HomeForm^ homeForm = gcnew HomeForm;
	homeForm->Show();
}

System::Void Project1::AccountWithdrawForm::button1_Click(System::Object^ sender, System::EventArgs^ e)
{
	AccountService accountService;
	auto currentAccount = State::getInstance();

	auto amountRequested = System::Double::Parse(this->Amount->Text);

	if (currentAccount->UserAccount.Amount < amountRequested) {
		MessageBox::Show("Недостаточно денег на счету",
			"Банк (оповещение)");
		return;
	}

	if (amountRequested > 10000) {
		MessageBox::Show("Невозможно снять средств на более 10 000р за одну операцию",
			"Банк (оповещение)");
		return;
	}

	if (accountService.Widthdraw(currentAccount->UserAccount, amountRequested)) {
		MessageBox::Show("Счет успешно обновлен. Средства сняты!", "Банк (оповещение)");

		this->Hide();
		HomeForm^ homeForm = gcnew HomeForm;
		homeForm->Show();
	}
}
