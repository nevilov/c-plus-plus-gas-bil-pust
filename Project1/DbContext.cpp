#include "DbContext.h"

QueryResponse DbContext::RunSql(std::string query)
{
    QueryResponse queryResponse;
    try
    {
        pqxx::connection connectionObject(ConnectionString.c_str());
        pqxx::work worker(connectionObject);
        pqxx::result response = worker.exec(query);

        worker.commit();

        queryResponse.IsSuccess = true;
        queryResponse.Result = response;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        queryResponse.IsSuccess = false;
        queryResponse.ErrorMessage = e.what();
    }

    return queryResponse;
}