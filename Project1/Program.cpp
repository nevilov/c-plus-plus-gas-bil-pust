#include "MainForm.h"
#include <windows.h>

using namespace System;
using namespace Project1;
using namespace System::Windows::Forms;

void systemInfo();

[STAThreadAttribute]
int main(array<System::String^>^ args) {
	systemInfo();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::EnableVisualStyles();
	MainForm form;
	Application::Run(% form);
}

void systemInfo() {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	//system("color 97");

	SetConsoleTextAttribute(hConsole,
		FOREGROUND_RED);

	printf("ПРОЕКТ - Банк\n");
	printf("Выполнил ст.гр.\n");

	printf(R"(          _ _             _____                 __           
    /\   | (_)           |_   _|               / _|          
   /  \  | |_ _ __ ___     | | _   _ ___ _   _| |_ _____   __
  / /\ \ | | | '_ ` _ \    | || | | / __| | | |  _/ _ \ \ / /
 / ____ \| | | | | | | |  _| || |_| \__ \ |_| | || (_) \ V / 
/_/    \_\_|_|_| |_| |_| |_____\__,_|___/\__,_|_| \___/ \_/  )");
	std::cout << "\n";

	printf("Язык C++\n");
	printf("WinForms C++/CLR\n");
	printf("PQXX Postgres DataBase library\n");
	printf("\n--CONSOLE OUTPUT--\n");
}
