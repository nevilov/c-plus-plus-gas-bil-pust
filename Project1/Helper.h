#pragma once
#include <iostream>
#include <sstream>
#include <ctime>
#include <iomanip>

class Helper {
public:
	time_t parseDateFromDb(const std::string& dateStr);
};
