#include <iostream>
#include <sstream>
#include <ctime>
#include <iomanip>

time_t parseDateFromDb(const std::string& dateStr) {
    std::tm tm = {};
    std::istringstream ss(dateStr);
    ss >> std::get_time(&tm, "%Y-%m-%d");

    if (ss.fail()) {
        std::cerr << "Parse failed\n";
        return -1;
    }

    return mktime(&tm);
}
