﻿#pragma once
#include "DepositService.h"

namespace Project1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for AddDepositForm
	/// </summary>
	public ref class AddDepositForm : public System::Windows::Forms::Form
	{
	public:
		AddDepositForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			DepositService depositTypeService;
			auto depositTypes = depositTypeService.GetDepositTypes();
			
			for (auto depositType : depositTypes) {
				if (&depositType != NULL) {
					auto managedString = gcnew String(depositType.Name.c_str());
					this->DepositTypes->Items->Add(managedString);
				}
			}
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AddDepositForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^ panel1;
	protected:
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Panel^ panel2;
	private: System::Windows::Forms::Label^ CurrentAmount;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Panel^ panel3;

	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ CurrentAccountId;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::ComboBox^ DepositTypes;


	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::DateTimePicker^ Date;

	private: System::Windows::Forms::TextBox^ RequestedAmount;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button1;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->CurrentAmount = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->DepositTypes = (gcnew System::Windows::Forms::ComboBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->Date = (gcnew System::Windows::Forms::DateTimePicker());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->RequestedAmount = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->CurrentAccountId = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->panel1->SuspendLayout();
			this->panel3->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->Controls->Add(this->panel2);
			this->panel1->Controls->Add(this->CurrentAmount);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Location = System::Drawing::Point(-5, 1);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(1579, 142);
			this->panel1->TabIndex = 0;
			// 
			// panel2
			// 
			this->panel2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel2->Location = System::Drawing::Point(322, 148);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(947, 896);
			this->panel2->TabIndex = 1;
			// 
			// CurrentAmount
			// 
			this->CurrentAmount->AutoSize = true;
			this->CurrentAmount->Font = (gcnew System::Drawing::Font(L"Impact", 18));
			this->CurrentAmount->Location = System::Drawing::Point(456, 39);
			this->CurrentAmount->Name = L"CurrentAmount";
			this->CurrentAmount->Size = System::Drawing::Size(51, 60);
			this->CurrentAmount->TabIndex = 1;
			this->CurrentAmount->Text = L"$";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Impact", 18));
			this->label1->Location = System::Drawing::Point(51, 39);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(380, 60);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Текущий баланс:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Impact", 18));
			this->label2->Location = System::Drawing::Point(134, 48);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(548, 60);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Форма открытия вклада";
			this->label2->Click += gcnew System::EventHandler(this, &AddDepositForm::label2_Click);
			// 
			// panel3
			// 
			this->panel3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel3->Controls->Add(this->button2);
			this->panel3->Controls->Add(this->button1);
			this->panel3->Controls->Add(this->DepositTypes);
			this->panel3->Controls->Add(this->label6);
			this->panel3->Controls->Add(this->Date);
			this->panel3->Controls->Add(this->label5);
			this->panel3->Controls->Add(this->RequestedAmount);
			this->panel3->Controls->Add(this->label4);
			this->panel3->Controls->Add(this->label3);
			this->panel3->Controls->Add(this->CurrentAccountId);
			this->panel3->Controls->Add(this->label2);
			this->panel3->Location = System::Drawing::Point(371, 145);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(846, 884);
			this->panel3->TabIndex = 2;
			// 
			// DepositTypes
			// 
			this->DepositTypes->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->DepositTypes->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18));
			this->DepositTypes->FormattingEnabled = true;
			this->DepositTypes->Location = System::Drawing::Point(228, 350);
			this->DepositTypes->Name = L"DepositTypes";
			this->DepositTypes->Size = System::Drawing::Size(430, 63);
			this->DepositTypes->TabIndex = 3;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->label6->Location = System::Drawing::Point(221, 578);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(245, 37);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Дата окончания";
			// 
			// Date
			// 
			this->Date->CalendarFont = (gcnew System::Drawing::Font(L"Impact", 18));
			this->Date->Location = System::Drawing::Point(240, 637);
			this->Date->MaxDate = System::DateTime(2100, 12, 31, 0, 0, 0, 0);
			this->Date->MinDate = System::DateTime(2023, 12, 10, 0, 0, 0, 0);
			this->Date->Name = L"Date";
			this->Date->Size = System::Drawing::Size(400, 31);
			this->Date->TabIndex = 8;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->label5->Location = System::Drawing::Point(221, 293);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(180, 37);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Вид вклада";
			// 
			// RequestedAmount
			// 
			this->RequestedAmount->Font = (gcnew System::Drawing::Font(L"Impact", 18));
			this->RequestedAmount->Location = System::Drawing::Point(228, 485);
			this->RequestedAmount->Name = L"RequestedAmount";
			this->RequestedAmount->Size = System::Drawing::Size(430, 66);
			this->RequestedAmount->TabIndex = 6;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->label4->Location = System::Drawing::Point(221, 433);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(117, 37);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Сумма";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->label3->Location = System::Drawing::Point(212, 156);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(208, 37);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Текущий счет";
			// 
			// CurrentAccountId
			// 
			this->CurrentAccountId->Font = (gcnew System::Drawing::Font(L"Impact", 18));
			this->CurrentAccountId->Location = System::Drawing::Point(219, 198);
			this->CurrentAccountId->Name = L"CurrentAccountId";
			this->CurrentAccountId->ReadOnly = true;
			this->CurrentAccountId->Size = System::Drawing::Size(430, 66);
			this->CurrentAccountId->TabIndex = 2;
			this->CurrentAccountId->Text = L"101";
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Impact", 14));
			this->button1->Location = System::Drawing::Point(270, 693);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(337, 79);
			this->button1->TabIndex = 10;
			this->button1->Text = L"Открыть вклад";
			this->button1->UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Impact", 14));
			this->button2->Location = System::Drawing::Point(270, 795);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(337, 79);
			this->button2->TabIndex = 11;
			this->button2->Text = L"Отмена";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// AddDepositForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1566, 1031);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->panel3);
			this->Name = L"AddDepositForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Создать депозит";
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void label2_Click(System::Object^ sender, System::EventArgs^ e) {
	}
};
}
