#include "AccountService.h"

AccountService::AccountService() {
	_dbContext = new DbContext();
}

Account AccountService::GetAccountByUser(int userId)
{
	std::string query = std::format(
		"SELECT id, createddate, bankid, amount FROM public.accounts where userid = {0}",
		userId);
	
	auto response = _dbContext->RunSql(query);

	if (!response.IsSuccess || response.Result.size() < 1) {
		return Account();
	}

	int id = pqxx::from_string<int>(response.Result[0][0]);
	std::string createdDate = pqxx::to_string(response.Result[0][1]);
	int bankId = pqxx::from_string<int>(response.Result[0][2]);
	double amount = pqxx::from_string<double>(response.Result[0][3]);

	return Account(id, userId, amount, bankId, createdDate);
}

bool AccountService::Replenish(Account account, double amount)
{
	if (amount > 10000) {
		return false;
	}
	auto newAmount = account.Amount + amount;

	auto query = std::format("update public.accounts set amount = {0} where id = {1}",
		newAmount, account.Id);
	auto queryResult = _dbContext->RunSql(query);

	if (queryResult.IsSuccess) {
		State::getInstance()->UserAccount.Amount = newAmount;
	}

	return queryResult.IsSuccess;
}

bool AccountService::Widthdraw(Account account, double amount)
{
	if (amount > 10000) {
		return false;
	}

	if (account.Amount < amount) {
		return false;
	}

	auto newAmount = account.Amount - amount;

	auto query = std::format("update public.accounts set amount = {0} where id = {1}",
		newAmount, account.Id);
	auto queryResult = _dbContext->RunSql(query);

	if (queryResult.IsSuccess) {
		State::getInstance()->UserAccount.Amount = newAmount;
	}

	return queryResult.IsSuccess;
}
