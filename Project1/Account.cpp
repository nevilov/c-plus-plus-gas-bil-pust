#include "Account.h"

Account::Account(int id, int userId, double amount, int bankId, std::string createdAt) 
	: EntityBase(id)
{
	UserId = userId;
	Amount = amount;
	BankId = bankId;
	CreatedAt = createdAt;
}

Account::Account(int userId, double amount, int bankId) 
{
	UserId = userId;
	Amount = amount;
	BankId = bankId;
}

Account::Account()
{
}
