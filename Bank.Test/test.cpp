#include "pch.h"
#include "../../Project1/Project1/UserService.h"
#include "../../Project1/Project1/UserService.cpp"
#include "../../Project1/Project1/Account.h"
#include "../../Project1/Project1/Account.cpp"
#include "../../Project1/Project1/User.h"
#include "../../Project1/Project1/User.cpp"
#include "../../Project1/Project1/Deposit.h"
#include "../../Project1/Project1/Deposit.cpp"
#include "../../Project1/Project1/DbContext.h"
#include "../../Project1/Project1/DbContext.cpp"
#include "../../Project1/Project1/AccountService.h"
#include "../../Project1/Project1/AccountService.cpp"
#include "../../Project1/Project1/State.h"
#include "../../Project1/Project1/State.cpp"
#include "../../Project1/Project1/QueryResponse.h"
#include "../../Project1/Project1/QueryResponse.cpp"
#include "../../Project1/Project1/Person.h"
#include "../../Project1/Project1/Person.cpp"
#include "../../Project1/Project1/EntityBase.h"
#include "../../Project1/Project1/EntityBase.cpp"
#include <pqxx/pqxx>

TEST(UserServiceTestCase, Auth_Fail_When_Wrong_Creds) {
	//Arrange
	UserService userService;
	std::string email = "nesuchestvuishiy_email@mail.ru";
	std::string password = "-1";

	//Act
	bool result = userService.Authorize(email, password);

	//Assert
	EXPECT_FALSE(result);
}

TEST(UserServiceTestCase, Auth_Success_When_Creds_Correct) {
	//Arrange
	UserService userService;
	std::string email = "a@mail.ru";
	std::string password = "1";

	//Act
	bool result = userService.Authorize(email, password);

	//Assert
	EXPECT_TRUE(result);

	auto actualUserEmail = State::getInstance()->CurrentUser.Email;
	EXPECT_EQ(actualUserEmail, email);
}

//Успешное пополнение до 10тыс
TEST(AccountServiceTestCase, Replenish_Success_When_Amount_Less_Than_10k) {
	//Arrange
	AccountService accountService;

	User testUser(101, "SUPER PUPER TEST", "1598623", "1", "test@mail.ru");
	double initAmount = 5000;
	Account account(25021, 101, initAmount, 1, "2023/01/01");
	State::getInstance()->SetUser(testUser);
	State::getInstance()->SetAccount(account);	

	//Act
	bool result = accountService.Replenish(account, 9999);

	//Assert
	EXPECT_TRUE(result);
	double expectedNewAmount = initAmount + 9999;
	EXPECT_EQ(expectedNewAmount, State::getInstance()->UserAccount.Amount);
}

//Неуспешное пополнение более 10тыс
TEST(AccountServiceTestCase, Replenish_Fail_When_Amount_Greater_Than_10k) {
	//Arrange
	AccountService accountService;

	User testUser(101, "SUPER PUPER TEST", "1598623", "1", "test@mail.ru");
	double initAmount = 5000;
	Account account(25021, 101, initAmount, 1, "2023/01/01");
	State::getInstance()->SetUser(testUser);
	State::getInstance()->SetAccount(account);

	//Act
	bool result = accountService.Replenish(account, 10001);

	//Assert
	EXPECT_FALSE(result);
}

//Неуспешное снятие более 10 тыс
TEST(AccountServiceTestCase, WithDraw_Fail_When_Amount_Greater_Than_10k) {
	//Arrange
	AccountService accountService;

	User testUser(101, "SUPER PUPER TEST", "1598623", "1", "test@mail.ru");
	double initAmount = 50000;
	Account account(25021, 101, initAmount, 1, "2023/01/01");
	State::getInstance()->SetUser(testUser);
	State::getInstance()->SetAccount(account);

	//Act
	bool result = accountService.Widthdraw(account, 10001);

	//Assert
	EXPECT_FALSE(result);
}

//Неуспешное снятие, недостаточно средств
TEST(AccountServiceTestCase, WithDraw_Fail_When_RequestedAmount_Greater_Than_Current) {
	//Arrange
	AccountService accountService;

	User testUser(101, "SUPER PUPER TEST", "1598623", "1", "test@mail.ru");
	double initAmount = 1000;
	Account account(25021, 101, initAmount, 1, "2023/01/01");
	State::getInstance()->SetUser(testUser);
	State::getInstance()->SetAccount(account);

	//Act
	bool result = accountService.Widthdraw(account, 9999);

	//Assert
	EXPECT_FALSE(result);
}

//Успешное снятие средств до 10тыс при достаточном количестве средств на счету
TEST(AccountServiceTestCase, WithDraw_Success_When_Amount_Less_Than_10k_And_Enough_Funds) {
	//Arrange
	AccountService accountService;

	User testUser(101, "SUPER PUPER TEST", "1598623", "1", "test@mail.ru");
	double initAmount = 9999;
	Account account(25021, 101, initAmount, 1, "2023/01/01");
	State::getInstance()->SetUser(testUser);
	State::getInstance()->SetAccount(account);

	//Act
	bool result = accountService.Widthdraw(account, 5000);

	//Assert
	EXPECT_TRUE(result);
	double expectedNewAmount = initAmount - 5000;
	EXPECT_EQ(expectedNewAmount, State::getInstance()->UserAccount.Amount);
}